#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "50 30\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  50)
        self.assertEqual(j, 30)

    def test_read_3(self):
        s = "399 33\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  399)
        self.assertEqual(j, 33)

    def test_read_4(self):
        s = "258 97\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  258)
        self.assertEqual(j, 97)

    def test_read_5(self):
        s = "50 30\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  50)
        self.assertEqual(j, 30)

    def test_read_6(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    def test_read_7(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 10)



    # ----
    # eval
    # ----
    

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(40, 363)
        self.assertEqual(v, 144)

    def test_eval_6(self):
        v = collatz_eval(498, 387)
        self.assertEqual(v, 142)

    def test_eval_7(self):
        v = collatz_eval(12, 275)
        self.assertEqual(v, 128)

    def test_eval_8(self):
        v = collatz_eval(1500, 4500)
        self.assertEqual(v, 238)
    
    def test_eval_9(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525)

    def test_eval_10(self):
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)
    
    def test_eval_11(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
            w = StringIO()
            collatz_print(w, 116, 479, 144)
            self.assertEqual(w.getvalue(), "116 479 144\n")

    def test_print_3(self):
            w = StringIO()
            collatz_print(w, 66, 150, 122)
            self.assertEqual(w.getvalue(), "66 150 122\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 156, 316, 131)
        self.assertEqual(w.getvalue(), "156 316 131\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")
    
    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 5, 5, 6)
        self.assertEqual(w.getvalue(), "5 5 6\n")

    def test_print_7(self):
        w = StringIO()
        collatz_print(w, 999999, 1, 525)
        self.assertEqual(w.getvalue(), "999999 1 525\n")
    


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("105 355\n211 358\n483 468\n251 327\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "105 355 144\n211 358 144\n483 468 129\n251 327 144\n")

    def test_solve_3(self):
        r = StringIO("595 526\n475 461\n699 699\n481 564\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "595 526 137\n475 461 129\n699 699 65\n481 564 142\n")
    
    def test_solve_4(self):
        r = StringIO("532 607\n471 425\n675 564\n414 666\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "532 607 137\n471 425 129\n675 564 145\n414 666 145\n")

    def test_solve_5(self):
        r = StringIO("5 5\n999999 1\n1 999999\n20 50\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 5 6\n999999 1 525\n1 999999 525\n20 50 112\n")
    
    def test_solve_6(self):
        r = StringIO("65135 867238\n98923 194654\n922565 874599\n418190 652960\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "65135 867238 525\n98923 194654 383\n922565 874599 476\n418190 652960 509\n")
    
    def test_solve_7(self):
        r = StringIO("356602 749059\n265289 935119\n490587 626332\n965836 209278\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "356602 749059 509\n265289 935119 525\n490587 626332 509\n965836 209278 525\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
